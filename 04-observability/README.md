# Задание
> Сервис покрыт логированием с использованием [zap](https://pkg.go.dev/go.uber.org/zap):
> * [x] новый запрос.
> * [x] ошибки при выполнении. 
> * [x] запуск и остановка приложения.  
> 
> Собраны метрики
> * [x] в docker-compose запущен prometheus, общение только через сеть docker 
> * [x] количество запросов не попадающих в формат *badrequest* 
> * [x] количество попыток входа *loginhandlecounter*
> * [x] количество провалившихся попыток входа *badlogin*
>
> Сервис пишет трассы:
> * [ ] обрабатывает входящий заголовок
> * [ ] создает спаны, отмечает спаны с ошибками
> * [ ] запущен jaeger на `:8082`
>
> Создание визуализации метрик:
> * [x] в docker-compose запущен grafana, доступный на `:8083` 

# Функционал
## Логирование запуска приложения
```console
04-observability-app-1         | 2023-11-19T18:59:07.068Z	INFO	app/app.go:52	starting application server	{"address": ":80"}
04-observability-app-1         | 2023-11-19T18:59:07.069Z	INFO	app/app.go:45	starting metrics server	{"handle": ":9000/metrics"}`
```
## Логирование любого запроса к API
```console
curl -X POST localhost:80/api/v1/login -d '{ "login": "test123", "password": "qwerty" }'
```
```console
04-observability-app-1         | 2023-11-19T19:08:56.695Z	INFO	httpadapter/adapter.go:208	request was received.	  {"path": "/api/v1/login"}
```
## Логирование ошибок
В представленных запросах, сообщение "this incedent will be reported" не пустая угроза :), это действие будет отражено в некоторой собираемой метрике
### Неверный формат данных
```console
curl -X POST localhost:80/api/v1/login
```
```console
04-observability-app-1         | 2023-11-19T19:06:08.886Z	INFO	httpadapter/adapter.go:208	request was received.	  {"path": "/api/v1/login"}
04-observability-app-1         | 2023-11-19T19:06:08.886Z	ERROR	httpadapter/adapter.go:92	request with bad format was reveived. this incedent will be reported.	  {"path": "/api/v1/login"}
```
### Неверные данные для входа
```console
curl -X POST localhost:80/api/v1/login -d '{ "login": "asd", "password": "a1" }'
```
```console
04-observability-app-1         | 2023-11-19T19:02:53.373Z	INFO	httpadapter/adapter.go:208	request was received.	  {"path": "/api/v1/login"}
04-observability-app-1         | 2023-11-19T19:02:53.375Z	ERROR	httpadapter/adapter.go:101	incorrect login or password received. this incedent will be reported.	  {"path": "/api/v1/login", "body": {"login":"asd","password":"a1"}}
```
## Визуализация собираемой статистики
После запуска `docker-compose` на [localhost:8083](localhost:8083) будет доступна визуализация grafana собираемых prometheus метрик

# Как это запустить?
## 1. Установите Docker и Docker Compose

Скачайте Docker и Docker Compose [с официального сайта](https://www.docker.com/)


## 2. Склонируйте исходный код
```console
git clone https://gitlab.com/gmvrpw/mipt-go-practice.git
```

## 3. Запустите Docker Compose
### Linux
```console
cd mipt-go-practice/04-observability
sudo docker compose up [-d]
```

## 4. Готово!
Воспользуйтесь всеми запросами!
