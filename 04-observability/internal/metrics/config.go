package metrics

type Config struct {
	ServeAddress  string `yaml:"serve_address"`
	MerticsHandle string `yaml:"metrics_handle"`
}
