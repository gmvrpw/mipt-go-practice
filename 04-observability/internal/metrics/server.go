package metrics

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type MetricsServer struct {
  config *Config
  server *http.Server
}

func (s *MetricsServer) Serve() error {
  r := chi.NewRouter()
  r.Handle("/metrics", promhttp.Handler())

  s.server = &http.Server{Addr: s.config.ServeAddress, Handler: r}

  return s.server.ListenAndServe()
}

func New(config *Config) *MetricsServer {
  return &MetricsServer{config: config}
}
