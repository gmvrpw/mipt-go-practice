package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Metrics struct {
  BadRequestFormat prometheus.Counter
  LoginHandleCounter prometheus.Counter
  BadLoginCounter prometheus.Counter
}

func NewMetrics() *Metrics {
  return &Metrics{
    BadRequestFormat: promauto.NewCounter(prometheus.CounterOpts{
      Namespace: "auth",
      Name: "badrequest",
      Help: "How many times was received request with wrong format",
    }), // Оценивает общее количество запросов с неправильным форматом
    LoginHandleCounter: promauto.NewCounter(prometheus.CounterOpts{
      Namespace: "auth",
      Name: "loginhandlecounter",
      Help: "How many times was the handle called",
    }), // Оценивает общее количество использования ручки входа
    BadLoginCounter: promauto.NewCounter(prometheus.CounterOpts{
      Namespace: "auth",
      Name: "badlogin",
      Help: "How many times was validation failed with correct input format",
    }), // Оценивает общее количество проваленных попыток входа при корректном вводе 
  }
}

