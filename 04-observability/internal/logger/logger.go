package logger

import (
	"go.uber.org/zap"
)

func NewLogger(config Config) (*zap.Logger, error) {
  if (config.Debug) {
    return zap.NewDevelopment();
  }
  return zap.NewProduction();
}
