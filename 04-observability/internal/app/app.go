package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/opentracing/opentracing-go"

	"04-observability/internal/httpadapter"
	"04-observability/internal/logger"
	"04-observability/internal/metrics"
	"04-observability/internal/repo/userrepo"
	"04-observability/internal/service"
	"04-observability/internal/service/authsvc"
)

type app struct {
	config *Config

  logger *zap.Logger
	tracer opentracing.Tracer

	authSerice  service.Auth
	httpAdapter httpadapter.Adapter
  metricsServer     *metrics.MetricsServer
}

func (a *app) Serve() error {
	done := make(chan os.Signal, 1)

	signal.Notify(done, syscall.SIGTERM, syscall.SIGINT)

	go func() {
    handlePath := fmt.Sprintf("%s%s", a.config.Metrics.ServeAddress, a.config.Metrics.MerticsHandle);
    a.logger.Info("starting metrics server", zap.String("handle", handlePath))
		if err := a.metricsServer.Serve(); err != nil && err != http.ErrServerClosed {
      a.logger.Error("metrics server closed with error", zap.String("error", err.Error()))
		}
	}()

	go func() {
    a.logger.Info("starting application server", zap.String("address", a.config.HTTP.ServeAddress))
		if err := a.httpAdapter.Serve(); err != nil && err != http.ErrServerClosed {
      a.logger.Error("application server closed with error", zap.String("error", err.Error()))
		}
	}()

	<-done
 
  a.logger.Info("shutting down...")
	a.Shutdown()
  a.logger.Info("stopped")

	return nil
}

func (a *app) Shutdown() {
	ctx, cancel := context.WithTimeout(context.Background(), a.config.App.ShutdownTimeout)
	defer cancel()

	a.httpAdapter.Shutdown(ctx)
}

func New(config *Config) (App, error) {
	pgxPool, err := initDB(context.Background(), &config.Database)
	if err != nil {
		return nil, err
	}

	userRepo, err := userrepo.New(&config.Auth, pgxPool)
	if err != nil {
		return nil, err
	}

  l, err := logger.NewLogger(logger.Config{Debug: config.App.Debug})
  if err != nil {
    return nil, err
  }

	authService := authsvc.New(&config.Auth, userRepo)

	a := &app{
		config:         config,
    logger:         l,
		authSerice:     authService,
		httpAdapter:    httpadapter.New(&config.HTTP, authService, l),
    metricsServer:  metrics.New(&config.Metrics),
	}

	return a, nil
}

func initDB(ctx context.Context, config *DatabaseConfig) (*pgxpool.Pool, error) {
	pgxConfig, err := pgxpool.ParseConfig(config.DSN)
	if err != nil {
		return nil, err
	}

	pool, err := pgxpool.ConnectConfig(ctx, pgxConfig)
	if err != nil {
		return nil, fmt.Errorf("unable to connect to database: %w", err)
	}

	// migrations

	m, err := migrate.New(config.MigrationsDir, config.DSN)
	if err != nil {
		return nil, err
	}

	if err := m.Down(); err != nil && err != migrate.ErrNoChange {
		return nil, err
	}

	if err := m.Up(); err != nil {
		return nil, err
	}

	return pool, nil
}
