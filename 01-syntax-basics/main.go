package main

import "fmt"

type Publisher int64
const (
  AST Publisher = iota
  Alpi
)

// Book
type BookId int64
type Book struct {
  name string
  author string
  publisher Publisher
}

// Storage
type identifiedBook struct {
  id BookId
  book Book
}

type storage interface {
  GetBook(id BookId) *Book
  AddBook(*identifiedBook)
}

// Storage based on slice
type sliceStorage struct {
  storage []*identifiedBook
}

func SliceStorage() sliceStorage {
  storage := sliceStorage{}
  storage.storage = []*identifiedBook{}

  return storage
}

func (s *sliceStorage) GetBook(id BookId) *Book {
  for _, identifiedBook := range s.storage {
    if identifiedBook.id == id {
      return &identifiedBook.book
    }
  }
  return nil
}

func (s *sliceStorage) AddBook(book *identifiedBook) {
  s.storage = append(s.storage, book)
}

// Storage based on map
type mapStorage struct {
  storage map[BookId]*identifiedBook
}

func MapStorage() mapStorage {
  storage := mapStorage{}
  storage.storage = map[BookId]*identifiedBook{}

  return storage
}

func (s *mapStorage) GetBook(id BookId) *Book {
  identifiedBook, ok := s.storage[id]
  if ok {
    return &identifiedBook.book
  }
  return nil
}

func (s *mapStorage) AddBook(book *identifiedBook) {
  s.storage[book.id] = book
}

// Bookworm
type bookworm interface {
  GetIdByName(name string) BookId
}

// Bookworm based on map
type consecutiveBookworm struct {
  mapping map[string]BookId
  lastId BookId
}

func ConsecutiveBookworm() consecutiveBookworm {
  bookworm := consecutiveBookworm{}
  bookworm.mapping = map[string]BookId{}
  bookworm.lastId = 0
  
  return bookworm
}

func (bw *consecutiveBookworm) GetIdByName(name string) BookId {
  id, ok := bw.mapping[name] 
  if ok {
    return id
  }

  bw.lastId += 1
  bw.mapping[name] = bw.lastId
  return bw.lastId
}

// Bookworm based on hashing
type hashBookworm struct {
  mod int64
  multiplier int64
}

func HashBookworm() hashBookworm {
  bookworm := hashBookworm{}
  bookworm.mod = 1000000 + 9
  bookworm.multiplier = 10561
  
  return bookworm
}

func (bw *hashBookworm) GetIdByName(name string) BookId {
  var hash int64 = 0
  var totalMultiplier int64 = 1
  for _, char := range name {
    hash += int64(char) * totalMultiplier % bw.mod
    hash %= bw.mod
    totalMultiplier *= bw.multiplier
    totalMultiplier %= bw.mod
  }

  return BookId(hash)
}


// Library
type Library struct {
  storage storage
  bookworm bookworm
}

func (l *Library) AddBook(book Book) {
  l.storage.AddBook(&identifiedBook{
  l.bookworm.GetIdByName(book.name),
    book,
  })
}

func (l *Library) GetBook(name string) *Book {
  return l.storage.GetBook(l.bookworm.GetIdByName(name))
}

func main() {
  warAndPeace := Book{"War and Peace", "Tolstoy", AST}
  masterAndMargarita := Book{"Master and Margarita", "Bulgakov", Alpi}

  // First test case
  fmt.Println("Test #1 (MapStorage + ConsecutiveBookworm)")  
  storageBasedOnMap := MapStorage()
  bookwormBasedOnСonsistency := ConsecutiveBookworm()

  MapConLibrary := Library{
    &storageBasedOnMap,
    &bookwormBasedOnСonsistency,
  }

  MapConLibrary.AddBook(warAndPeace)
  MapConLibrary.AddBook(masterAndMargarita)

  fmt.Println("should return \"War and Peace\": ", *MapConLibrary.GetBook("War and Peace"))  
  fmt.Println("should return \"Master and Margarita\": ", *MapConLibrary.GetBook("Master and Margarita"))   
  fmt.Println("should return nil: ", MapConLibrary.GetBook("Fathers and Sons"))  

  fmt.Println()

  // Second test case
  fmt.Println("Test #2 (SliceStorage + HashBookworm)")  
  storageBasedOnSlice := SliceStorage()
  bookwormBasedOnHash := HashBookworm()

  SliceHashLibrary := Library{
    &storageBasedOnSlice,
    &bookwormBasedOnHash,
  }

  SliceHashLibrary.AddBook(masterAndMargarita)
  SliceHashLibrary.AddBook(warAndPeace)

  fmt.Println("should return \"War and Peace\": ", *SliceHashLibrary.GetBook("War and Peace"))  
  fmt.Println("should return \"Master and Margarita\": ", *SliceHashLibrary.GetBook("Master and Margarita"))   
  fmt.Println("should return nil: ", SliceHashLibrary.GetBook("Fathers and Sons"))  
}
