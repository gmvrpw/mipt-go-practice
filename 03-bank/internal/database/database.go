package database

type DataBase interface {
  GetUserById(id int64) User
}
