package database

type User interface {
  GetId() int64;
  GetBalance() int;
  SetBalance(balance int) int;
}
