package runtime

type User struct {
  id int64;
  balance int;
}
func NewUser(id int64, balance int) *User {
  return &User{
    id: id,
    balance: balance,
  }
}

func (user *User) GetId() int64 {
  return user.id;
}

func (user *User) GetBalance() int {
  return user.balance;
}

func (user *User) SetBalance(balance int) int {
  user.balance = balance;
  return balance;
}
