package runtime

import (
	"bank/internal/database"
)

type RuntimeDataBase struct {
  users []User;
}
func NewRuntimeDataBase() *RuntimeDataBase {
  return &RuntimeDataBase{
    users: []User{{id: 1, balance: 2000}, {id: 2, balance: 50000}},
  };
}

func (db *RuntimeDataBase) GetUserById(id int64) database.User {
  for index := range db.users {
    if db.users[index].id == id {
      return &db.users[index]
    }
  }
  return nil;
}
