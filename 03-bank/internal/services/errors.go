package services

const (
  OK = "ok"
  USER_NOT_FOUND = "notFound"
  SENDER_NOT_FOUND = "senderNotFound"
  RECEIVER_NOT_FOUND = "receiverNotFound"
  NOT_ENOUGH_MONEY = "notEnoughMoney"
  SELF_TRANSACTING = "selfTransaction"
  BAD_VALUE_TRANSACTION = "negativeValueTransaction"
)
