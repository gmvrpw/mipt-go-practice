package services

import (
	"bank/internal/repositories"
	"errors"
)

type BalanceService struct {
  repository *repositories.UserRepository;
}
func NewBalanceService(repository *repositories.UserRepository) *BalanceService {
  return &BalanceService{
    repository: repository,
  }
}

func (service *BalanceService) Get(userId int64) (int, error) {
  user := service.repository.FindUserById(userId);

  if user == nil {
    return 0, errors.New(USER_NOT_FOUND)
  }

  return user.GetBalance(), nil;
}

func (service *BalanceService) Patch(value int, userId int64) (int, error) {
  user := service.repository.FindUserById(userId);

  if user == nil {
    return 0, errors.New(USER_NOT_FOUND)
  }

  balance := user.GetBalance()
  if balance + value < 0 {
    return balance, errors.New(NOT_ENOUGH_MONEY)
  }

  user.SetBalance(balance + value);

  return balance + value, nil;
}

func (service *BalanceService) Transaction(value int, senderId int64, receiverId int64) (int, error) {
  if senderId == receiverId {
    return 0, errors.New(SELF_TRANSACTING)
  }
  if value <= 0 {
    return 0, errors.New(BAD_VALUE_TRANSACTION)
  }

  sender := service.repository.FindUserById(senderId)
  if sender == nil {
    return 0, errors.New(SENDER_NOT_FOUND)
  }
  
  receiver := service.repository.FindUserById(receiverId)
  if receiver == nil {
    return 0, errors.New(RECEIVER_NOT_FOUND)
  }

  senderBalance := sender.GetBalance()
  if senderBalance - value < 0 {
    return senderBalance, errors.New(NOT_ENOUGH_MONEY)
  }

  sender.SetBalance(senderBalance - value)
  receiver.SetBalance(receiver.GetBalance() + value);

  return senderBalance - value, nil;
}


