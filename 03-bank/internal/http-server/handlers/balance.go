package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bank/internal/services"
	"bank/models"
)

type BalanceHandler struct {
  service *services.BalanceService
}
func NewBalanceHandler(service *services.BalanceService) *BalanceHandler {
  return &BalanceHandler{
    service: service,
  }
}

func (handler *BalanceHandler) Handler(res http.ResponseWriter, req *http.Request) {
  switch method := req.Method; method {
    case "GET":
      handler.get(res, req)
    case "PATCH":
      handler.patch(res, req)
  }
}

func (handler *BalanceHandler) get(res http.ResponseWriter, req *http.Request) {
  userId, err := strconv.ParseInt(req.URL.Query().Get("user_id"), 10, 64);
  if (err != nil) {
    userId = 0;
  }

  balance, err := handler.service.Get(userId)
  if (err != nil) {
    switch err.Error() {
      case services.USER_NOT_FOUND: SendUserNotFound(res, userId)
      default: SendInternal(res, err)
    }
    return
  }

  responceBody, _ := json.Marshal(models.Balance{
    Balance: balance,
  })
  
  res.WriteHeader(http.StatusOK)
  res.Header().Set("Content-Type", "application/json")
  _, _ = res.Write(responceBody)

  return;
}

func (handler *BalanceHandler) patch(res http.ResponseWriter, req *http.Request) {
  requestBody := models.PatchBalance{}
  decoder := json.NewDecoder(req.Body)
  err := decoder.Decode(&requestBody)
  if err != nil {
    SendParsingError(res) 
    return
  }

  balance, err := handler.service.Patch(requestBody.Value, requestBody.UserId);

  if err != nil {
    switch err.Error() {
      case services.USER_NOT_FOUND: SendUserNotFound(res, requestBody.UserId)
      case services.NOT_ENOUGH_MONEY: SendNotEnoughMoney(res, balance)  
      default: SendInternal(res, err)  
    }
    return
  }

  responseBody, _ := json.Marshal(models.Balance{
    Balance: balance,
  }) 

  res.WriteHeader(http.StatusOK)
  res.Header().Set("Content-Type", "application/json")
  _, _ = res.Write(responseBody)

  return
}
