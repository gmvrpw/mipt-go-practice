package handlers

import (
	"encoding/json"
	"net/http"

	"bank/internal/services"
	"bank/models"
)

type TransactionHandler struct {
  service *services.BalanceService
}
func NewTransactionHander(service *services.BalanceService) *TransactionHandler {
  return &TransactionHandler{
    service: service,
  }
}

func (handler *TransactionHandler) Handler(res http.ResponseWriter, req *http.Request) {
  switch method := req.Method; method {
    case "POST":
      handler.post(res, req)
  }
}

func (handler *TransactionHandler) post(res http.ResponseWriter, req *http.Request) {
  requestBody := models.Transaction{}
  decoder := json.NewDecoder(req.Body)
  err := decoder.Decode(&requestBody)
  if err != nil {
    SendParsingError(res)
    return
  }

  balance, err := handler.service.Transaction(requestBody.Value, requestBody.SenderId, requestBody.ReceiverId);
  if err != nil {
    switch err.Error() {
      case services.SELF_TRANSACTING: SendSelfTransacting(res)
      case services.BAD_VALUE_TRANSACTION: SendBadValueTransaction(res, requestBody.Value)
      case services.SENDER_NOT_FOUND: SendUserNotFound(res, requestBody.SenderId) 
      case services.RECEIVER_NOT_FOUND: SendUserNotFound(res, requestBody.ReceiverId) 
      case services.NOT_ENOUGH_MONEY: SendNotEnoughMoney(res, balance)
      default: SendInternal(res, err)
    }
    return
  }

  responseBody, _ := json.Marshal(models.Balance{
    Balance: balance,
  }) 

  res.WriteHeader(http.StatusOK)
  res.Header().Set("Content-Type", "application/json")
  _, _ = res.Write(responseBody)

  return
}
