package handlers

import (
	"fmt"
	"net/http"
)

func SendInternal(res http.ResponseWriter, err error) {
  http.Error(res, err.Error(), http.StatusInternalServerError);
}

func SendParsingError(res http.ResponseWriter) {
  http.Error(res, "Некоректный формат ввода", http.StatusBadRequest);
}

func SendSelfTransacting(res http.ResponseWriter) {
  http.Error(res, "Отправитель не может совпадать с получателем", http.StatusBadRequest);
}

func SendBadValueTransaction(res http.ResponseWriter, value int) {
  http.Error(res, fmt.Sprintf("Сумма %d недопустима для перевода", value), http.StatusBadRequest);
}

func SendUserNotFound(res http.ResponseWriter, userId int64) {
  http.Error(res, fmt.Sprintf("Пользователь %d не найден", userId), http.StatusBadRequest);
}

func SendNotEnoughMoney(res http.ResponseWriter, balance int) {
  http.Error(res, fmt.Sprintf("Недостаточно средств, баланс: %d", balance), http.StatusBadRequest);
}
