package repositories

import "bank/internal/database"

type UserRepository struct {
	db database.DataBase;
}
func NewUserRepository(db database.DataBase) *UserRepository {
  return &UserRepository{
    db: db,
  }
}

func (repository *UserRepository) FindUserById(id int64) database.User {
  return repository.db.GetUserById(id)
}
