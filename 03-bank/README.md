# Задание
> Сервис позволяет работать с балансами пользователей и предоставляет 3 функции:
> 1. [x] Зачислить средства на баланс.
> 2. [x] Перевести средства с одного баланса на другой.
> 3. [x] Посмотреть остаток на балансе.  
> 
> Дополнительные условия:
> * [x] баланс не может уйти в минус
> * [x] нельзя переводить самому себе

# Реализация
Актуальная реализация базы данных - это slice поиск по которому реализован наивным перебором за O(n)  
В базе данных создано два пользователя:
```json
{
  db: [
    {
      "id": 1,
      "balance": 2000
    },
    {
      "id": 2,
      "balance": 50000
    }
  ]
}
```
## API
### Получение баланса пользователя
#### Пользователь найден
```console
curl -X GET http://localhost:5000/balance?user_id=1 
```
```json
{
  "balance": 2000
}
```
#### Пользователь *не* найден
```console
curl -X GET http://localhost:5000/balance?user_id=0 
```
```plain
Пользователь 0 не найден
```
### Изменение баланса
#### Баланса достаточно
```console
curl -X PATCH http://localhost:5000/balance -H "Content-Type: application/json" -d '{ "user_id": 1, "value": -1000}'  
```
```json
{
  "balance": 1000
}
```
#### Баланса *не* достаточно
```console
curl -X PATCH http://localhost:5000/balance -H "Content-Type: application/json" -d '{ "user_id": 1, "value": -3000}'  
```
```plain
Недостаточно средств, баланс: 2000
```

### Транзакция
#### Пользователи не совпадают, размер перевода неотрицательный 
```console
curl -X POST http://localhost:5000/transaction -H "Content-Type: application/json" -d '{ "sender_id": 1, "receiver_id": 2, "value": 500}'  
```
```json
{
  "balance": 1500
}
```
#### Пользователи совпадают 
```console
curl -X POST http://localhost:5000/transaction -H "Content-Type: application/json" -d '{ "sender_id": 1, "receiver_id": 1, "value": 500}'  
```
```plain
Отправитель не может совпадать с получателем
```

#### Размер перевода отрицательный 
```console
curl -X POST http://localhost:5000/transaction -H "Content-Type: application/json" -d '{ "sender_id": 1, "receiver_id": 2, "value": -500}'  
```
```plain
Сумма -500 недопустима для перевода
```

# Как это запустить?
## 1. Установите Docker и Docker Compose

Скачайте Docker и Docker Compose [с официального сайта](https://www.docker.com/)


## 2. Склонируйте исходный код
```console
git clone https://gitlab.com/gmvrpw/mipt-go-practice.git
```

## 3. Запустите Docker Compose
### Linux
```console
cd mipt-go-practice
sudo docker compose up [-d]
```

## 3. Запустите Docker Compose
### Linux
```console
sudo docker compose up [-d]
```

## 4. Готово!
Воспользуйтесь всеми запросами!
