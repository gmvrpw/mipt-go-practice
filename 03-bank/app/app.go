package app

import (
	"context"
	"fmt"
	"net/http"

	"bank/internal/config"
	database "bank/internal/database/runtime"
	"bank/internal/http-server/handlers"
	"bank/internal/repositories"
	"bank/internal/services"
)

type App struct {
  server *http.Server

  balanceService *services.BalanceService;
}

func NewApp(cfg *config.Config) *App {
  db := database.NewRuntimeDataBase()
  userRepository := repositories.NewUserRepository(db)

  balanceService := services.NewBalanceService(userRepository)

  fmt.Printf("Running on port: %d", cfg.Server.Port)
  return &App{
    balanceService: balanceService,
    server: &http.Server{
      Addr: fmt.Sprintf(":%d", cfg.Server.Port),
      Handler: getHandlers(balanceService),
    },
  }
}

func (app *App) Run() {
  go func() {
    err := app.server.ListenAndServe()
    if (err != nil) {
      fmt.Println(err)
    }
  }()
}

func (app *App) Stop(ctx context.Context) {
  fmt.Println(app.server.Shutdown(ctx))
}

func getHandlers(balanceService *services.BalanceService) http.Handler {
  balanceHandler := handlers.NewBalanceHandler(balanceService)
  transactionHandler := handlers.NewTransactionHander(balanceService)

  mux := http.NewServeMux()
  mux.HandleFunc("/balance", balanceHandler.Handler);
  mux.HandleFunc("/transaction", transactionHandler.Handler);
  
  return mux;
}
