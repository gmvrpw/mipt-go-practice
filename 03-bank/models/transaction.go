package models

type Transaction struct {
  Value int `json:"value"`;
  SenderId int64 `json:"sender_id"`;
  ReceiverId int64 `json:"receiver_id"`;
}
