package models

type Balance struct {
  Balance int `json:"balance"`
}

type PatchBalance struct {
  UserId int64 `json:"user_id"`
  Value int `json:"value"`
}
