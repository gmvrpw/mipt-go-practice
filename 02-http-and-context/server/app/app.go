package app

import (
	"context"
	"fmt"
	"net/http"

	"server/internal/config"
	"server/internal/http-server/handlers"
	"server/internal/services"
)

type App struct {
  server *http.Server

  metaService *services.MetaService
}

func NewApp(cfg *config.Config) *App {
  metaService := services.NewMetaService()
  decodeService := services.NewDecodeService()
  hardOpService := services.NewHardOpService(cfg.SleepTimeStart,
                                             cfg.SleepTimeStop)

  return &App{
    metaService: metaService,
    server: &http.Server{
      Addr: fmt.Sprintf("%s:%d", cfg.Server.Host, cfg.Server.Port),
      Handler: getHandlers(metaService, decodeService, hardOpService),
    },
  }
}

func (app *App) Run() {
  go func() {
    err := app.server.ListenAndServe()
    if (err != nil) {
      fmt.Println(err)
    }
  }()
}

func (app *App) Stop(ctx context.Context) {
  fmt.Println(app.server.Shutdown(ctx))
}

func getHandlers(metaService *services.MetaService,
                 decodeService *services.DecodeService,
                 hardOpService *services.HardOpService) http.Handler {
  versionHandler := handlers.NewVersionHandler(metaService)
  decodeHandler := handlers.NewDecodeHandler(decodeService)
  hardOpHandler := handlers.NewHardOpHander(hardOpService)

  mux := http.NewServeMux()
  mux.HandleFunc("/version", versionHandler.Handler);
  mux.HandleFunc("/decode", decodeHandler.Handler);
  mux.HandleFunc("/hard-op", hardOpHandler.Handler);

  return mux;
}
