package services

import (
  "math/rand"
  "time"
)

type HardOpService struct {
  start int
  stop int
}

func NewHardOpService(start int, stop int) *HardOpService {
  return &HardOpService{
    start: start,
    stop: stop,
  }
}

func (service *HardOpService) GetRandomResponseCode() int {
  sleepTime := service.start + rand.Intn(service.stop - service.start);
  time.Sleep(time.Duration(sleepTime) * time.Second)

  if rand.Float32() < 0.5 {
    return 500 + rand.Intn(100)
  }
  return 200; 
}
