package services

import (
	"encoding/base64"
	"fmt"
)

type DecodeService struct {
}

func NewDecodeService() *DecodeService {
  return &DecodeService{}
}

func (service *DecodeService) Decode(source string) string {
  decoded, err := base64.StdEncoding.DecodeString(source)

  if err != nil {
    fmt.Println(err.Error())
  }

  return string(decoded)
}
