package services

type MetaService struct {
  version string
}

func NewMetaService() *MetaService {
  return &MetaService{
    version: "1.0.0",
  }
}

func (service *MetaService) Version() string {
  return service.version;
}
