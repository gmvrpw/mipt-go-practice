package config

import (
	"encoding/json"
	"os"
)

type Server struct {
  Host string `json:"host"`
  Port int `json:"port"`
}

type Config struct {
  Server Server
  SleepTimeStart int `json:"sleepTimeStart"`
  SleepTimeStop int `json:"sleepTimeStop"`
}

func NewConfig(path string) (*Config, error) {
  data, err := os.ReadFile(path)
  if err != nil {
    return nil, err
  }

  var cfg Config
  err = json.Unmarshal(data, &cfg)
  if err != nil {
    return nil, err
  }

  return &cfg, nil
}
