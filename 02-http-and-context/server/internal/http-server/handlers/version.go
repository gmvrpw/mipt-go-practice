package handlers

import (
  "server/internal/services"

  "net/http"
)

type VersionHandler struct {
  service *services.MetaService
}

func NewVersionHandler(service *services.MetaService) *VersionHandler {
  return &VersionHandler{
    service: service,
  }
}

func (handler *VersionHandler) Handler(res http.ResponseWriter, req *http.Request) {
  switch method := req.Method; method {
    case "GET":
      handler.get(res, req)
  }
}

func (handler *VersionHandler) get(res http.ResponseWriter, req *http.Request) {
  res.WriteHeader(http.StatusOK)
  res.Header().Set("Content-Type", "plain/text")
  _, _ = res.Write([]byte(handler.service.Version()))

  return
}
