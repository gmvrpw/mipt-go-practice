package handlers

import (
	"encoding/json"
	"net/http"

	"server/internal/services"
	"server/models"
)

type DecodeHandler struct {
  service *services.DecodeService
}

func NewDecodeHandler(service *services.DecodeService) *DecodeHandler {
  return &DecodeHandler{
    service: service,
  }
}

func (handler *DecodeHandler) Handler(res http.ResponseWriter, req *http.Request) {
  switch method := req.Method; method {
    case "POST":
      handler.post(res, req)
  }
}

func (handler *DecodeHandler) post(res http.ResponseWriter, req *http.Request) {
  decodeModel := models.Decode{}

  decoder := json.NewDecoder(req.Body)
  err := decoder.Decode(&decodeModel)
  if err != nil {
    http.Error(res, err.Error(), http.StatusBadRequest)
    return
  }

  decoded := handler.service.Decode(decodeModel.InputString)
  decodeModel.OutputString = decoded

  responseJson, _ := json.Marshal(decodeModel) 

  res.WriteHeader(http.StatusOK)
  res.Header().Set("Content-Type", "application/json")
  _, _ = res.Write(responseJson)

  return
}
