package handlers

import (
	"net/http"

	"server/internal/services"
)

type HardOpHandler struct {
  service *services.HardOpService
}

func NewHardOpHander(service *services.HardOpService) *HardOpHandler {
  return &HardOpHandler{
    service: service,
  }
}

func (handler *HardOpHandler) Handler(res http.ResponseWriter, req *http.Request) {
  switch method := req.Method; method {
    case "GET":
      handler.get(res, req)
  }
}

func (handler *HardOpHandler) get(res http.ResponseWriter, req *http.Request) {
  res.WriteHeader(handler.service.GetRandomResponseCode())

  return
}
