package models

type Decode struct {
  InputString string `json:"inputString"`
  OutputString string `json:"outputString"`
}
