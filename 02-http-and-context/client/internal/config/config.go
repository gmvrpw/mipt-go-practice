package config

import (
	"encoding/json"
	"fmt"
	"os"
  "net/url"
)

type Target struct {
  Url url.URL;
  Scheme string `json:"scheme"`
  Host string `json:"host"`
  Port int `json:"port"`
}

type Config struct {
  Target Target
  Timeout int `json:"timeout"`
}

func constructUrl(protocol string, host string, port int) string {
  return fmt.Sprintf("%s://%s:%d", protocol, host, port)
}

func NewConfig(path string) (*Config, error) {
  data, err := os.ReadFile(path)
  if err != nil {
    return nil, err
  }

  var cfg Config
  err = json.Unmarshal(data, &cfg)
  if err != nil {
    return nil, err
  }

  cfg.Target.Url = url.URL{
    Scheme: cfg.Target.Scheme,
    Host: fmt.Sprintf("%s:%d", cfg.Target.Host, cfg.Target.Port),
  };

  return &cfg, nil
}
