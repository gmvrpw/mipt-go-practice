package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
	"time"

	"client/internal/config"
	"client/models"
)

func main() {
  var cfgPath string
  flag.StringVar(&cfgPath, "cfg", ".config.json", "config path")
  flag.Parse()

  cfg, err := config.NewConfig(cfgPath)
  if err != nil {
    fmt.Printf("fatal: config parse error, %s (%s)", err.Error(), cfgPath)
  }

  GetBackendVersion(cfg)
  DecodeBase64(cfg)
  GetRandomResponseStatusCode(cfg)
}

func GetBackendVersion(cfg *config.Config) {
  res, err := http.Get(cfg.Target.Url.JoinPath("version").String())
  if err != nil {
    fmt.Println(err.Error())
    return
  }
  
  defer res.Body.Close()
  body, _ := io.ReadAll(res.Body)
  fmt.Printf("Backend version: %s\n", string(body))
}

func DecodeBase64(cfg *config.Config) {
  var base64 string;
  fmt.Print("Введите base64 строку (default):")
  fmt.Scanf("%s", &base64)

  if (base64 == "") {
    base64 = "dGVzdCBtZXNzYWdlIGluIGJhc2U2NA==" 
  }

  decode := models.Decode{InputString: base64}

  jsonRequest, _ := json.Marshal(decode)
  res, err := http.Post(cfg.Target.Url.JoinPath("decode").String(),
                        "application/json",
                        bytes.NewBuffer(jsonRequest))
  if err != nil {
    fmt.Println(err.Error())
    return
  }
 
  decoder := json.NewDecoder(res.Body);
  decoder.Decode(&decode)
  
  fmt.Println(decode.OutputString)
}

func GetRandomResponseStatusCode(cfg *config.Config) {
  fmt.Print("Пытаюсь получить случайный код ответа... ")
  
  req, err := http.NewRequest(http.MethodGet, cfg.Target.Url.JoinPath("hard-op").String(), nil)
  if (err != nil) {
    fmt.Print(err.Error())
  }

  ctx, cancel := context.WithTimeout(req.Context(), time.Duration(cfg.Timeout) * time.Second)
  defer cancel()

  req = req.WithContext(ctx)

  res, err := http.DefaultClient.Do(req)

  if (err != nil) {
    fmt.Println(err.Error())
    return
  }

  fmt.Print(res.StatusCode)
}
